# Installing Home-brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Updates
brew update

# Installing a newer version of bash
brew install bash

# Putting the location of the shell into the list of allowed shells
echo $(brew --prefix)/bin/bash | sudo tee -a /private/etc/shells

# Change the default bash
sudo chpass -s /usr/local/bin/bash

# Bread crumbs in the Path Bar
defaults write com.apple.finder ShowPathbar -bool true

# Show hidden files
defaults write com.apple.finder AppleShowAllFiles YES

# Show status bar
defaults write com.apple.finder ShowStatusBar -bool true

# Installing Docutils
brew install dockutil

# Deleting apps from the doc
dockutil --remove 'Messages'
dockutil --remove 'Safari'
dockutil --remove 'Mail'
dockutil --remove 'Maps'
dockutil --remove 'Photos'
dockutil --remove 'FaceTime'
dockutil --remove 'Contacts'
dockutil --remove 'Reminders'
dockutil --remove 'TV'
dockutil --remove 'Music'
dockutil --remove 'Podcasts'
dockutil --remove 'System Preferences'
dockutil --remove 'App Store'

# Installing basic dev utils
brew install \
  git \
  wget \
  fzf

brew install --cask \
  visual-studio-code \
  google-chrome \
  sublime-text \
  rectangle \
  iterm2 \
  slack \
  spotify \
  vlc

dockutil --add '/Applications/Google Chrome.app' --after 'Launchpad'
dockutil --add '/Applications/Visual Studio Code.app' --after 'Google Chrome'
dockutil --add '/Applications/Slack.app' --after 'Visual Studio Code'
dockutil --add '/Applications/Spotify' --after 'Slack'

# Installing My Dev Stack
brew install \
  elixir \
  postgresql@14 \
  postico \
  docker \
  redis \
  mysql

initdb --locale=C -E UTF-8 /usr/local/var/postgresql@14

brew services start postgresql
brew services start redis
brew services restart mysql

mysql_secure_installation

# Elixir's Hex and Rebar
mix local.hex
mix local.rebar --force